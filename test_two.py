from hdfs import InsecureClient
import time
from numpy import true_divide
from pathlib import Path
import cv2
import numpy as np
import io
from PIL import Image
import base64
from tqdm import tqdm


def write_file(client: InsecureClient, local_path: str, hdfs_path: str = None) -> None:
    cwd = Path(local_path)

    for file in cwd.rglob('*.*'):
        client.upload(hdfs_path + file.name,
                      str(file),
                      overwrite=True)
        print(f'uploading {file.name}')


def write_frame(client: InsecureClient, frames: dict, hdfs_path: str) -> None:
    for name_frame, frame in frames.items():
        client.upload(hdfs_path + name_frame,
                      str(frame),
                      overwrite=True)


def cut_video(client: InsecureClient, path_to_video: str, fps: float = 0.5) -> None:
    name_video = path_to_video.split('/')[-1].split('.')[0]

    cap = cv2.VideoCapture(path_to_video)
    t = time.time()

    i = 0
    time_one = fps

    while True:
        images = dict()
        ret, frame = cap.read()
        try:
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            if time.time() - t >= time_one:
                i += 1
                t = time.time()
                images[f"frame_{i}.jpg"] = img
        except Exception as e:
            continue


    write_frame(client=client, frames=name_video, hdfs_path=f'{name_video}/')


root_path = '/'
client = InsecureClient(url='http://127.0.0.1:9870', user='asemenov', root=root_path)

# start_time = time.time()
# cut_video(client, path_to_video='./files_example/Soccer_game.mp4', fps=0.9)
# times = time.time() - start_time
# print(f'Time for download: {times}')

# print(client)
# print(client.list(f'/'))
#
# write_file(client=client, local_path='./files_example', hdfs_path='/')

with client.read(f'/Soccer_game.mp4', chunk_size=128) as reader:
    video = list()
    for chunk in tqdm(reader):
        video.append(chunk)
    # video = reader.read()
    # print(video)
    # with open('./files_example/soccer_game_hdfs.mp4', 'wb') as wfile:
    #     wfile.write(video)


# for file in client.list(f'/'):
#     # if 'jpg' in file:
#     #     with client.read(f'{file}') as reader:
#     #         image_bytes = bytearray(reader.read())
#     #         image = Image.open(io.BytesIO(image_bytes))
#     #         image.show()
#     if 'MP4' in file:
#         print('mp4')
#         start_time = time.time()
#         with client.read(f'{file}') as reader:
#             print(type(reader.read()))
#             video = reader.read()
#             print(reader.read())
#             with open('text.txt', 'w') as f:
#                 f.write(video)
#             # video_bytes = base64.b64decode(reader.read())
#             # with open('./files_example/soccer_game_hdfs.mp4', 'wb') as wfile:
#             #     wfile.write(video)
#             #
# print(f'Time for download video: {time.time() - start_time}')
